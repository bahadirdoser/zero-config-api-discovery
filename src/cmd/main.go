package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"os"
	"strconv"
	"strings"
	"zero-config-api-discovery/pkg/config"
	"zero-config-api-discovery/pkg/scanner"
	"zero-config-api-discovery/pkg/storage"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type ProxyConfig struct {
	Enabled  bool   `json:"enabled"`
	Address  string `json:"address"`
	Port     int    `json:"port"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type HostConfig struct {
	Hostname string      `json:"hostname"`
	Proxy    ProxyConfig `json:"proxy"`
}

type PortsConfig struct {
	Include    []int `json:"include"`
	Exclude    []int `json:"exclude"`
	ScanCommon bool  `json:"scancommon"`
}

type PathsConfig struct {
	Include    []string `json:"include"`
	Exclude    []string `json:"exclude"`
	ScanCommon bool     `json:"scancommon"`
}

type ScanInput struct {
	Hostnames []HostConfig `json:"hostnames"`
	Ports     PortsConfig  `json:"ports"`
	Paths     PathsConfig  `json:"paths"`
}

var logger *zap.Logger

func setupLogger(cfg *config.LogConfig) (*zap.Logger, error) {
	loglevel := zapcore.InfoLevel
	cfgLevel := cfg.Level

	if cfgLevel != "" {
		if err := loglevel.UnmarshalText([]byte(cfgLevel)); err != nil {
			return nil, err
		}
	}
	switch cfgLevel {
	case "debug":
		loglevel = zapcore.DebugLevel
	case "info":
		loglevel = zapcore.InfoLevel
	case "warn":
		loglevel = zapcore.WarnLevel
	case "error":
		loglevel = zapcore.ErrorLevel
	default:
		break
	}

	config := zap.Config{
		Level:            zap.NewAtomicLevelAt(loglevel),
		Development:      false,
		Encoding:         "json",
		OutputPaths:      cfg.OutputPaths,
		ErrorOutputPaths: cfg.ErrorOutputPaths,
		EncoderConfig: zapcore.EncoderConfig{
			TimeKey:        "time",
			LevelKey:       "level",
			NameKey:        "logger",
			CallerKey:      "caller",
			MessageKey:     "msg",
			StacktraceKey:  "stacktrace",
			LineEnding:     zapcore.DefaultLineEnding,
			EncodeLevel:    zapcore.LowercaseLevelEncoder,
			EncodeTime:     zapcore.ISO8601TimeEncoder,
			EncodeDuration: zapcore.SecondsDurationEncoder,
			EncodeCaller:   zapcore.ShortCallerEncoder,
		},
	}

	logger, err := config.Build()
	if err != nil {
		return nil, err
	}
	return logger, nil
}

func main() {
	appConfig, err := config.LoadConfig("../app-config.json")
	if err != nil {
		logger.Fatal("failed to load configuration", zap.Error(err))
	}

	logger, err := setupLogger(&appConfig.Logging)
	if err != nil {
		logger.Fatal("failed to setup logger", zap.Error(err))
	}
	defer logger.Sync()

	logger.Info("Logger configured and started")

	inputJSON := flag.String("input", "", "JSON string with scan input")
	flag.Parse()

	if *inputJSON == "" {
		logger.Fatal("Input JSON is required. Use the -input flag to provide it.")
	}

	var scanInput ScanInput
	err = json.Unmarshal([]byte(*inputJSON), &scanInput)
	if err != nil {
		logger.Fatal("Failed to parse input JSON", zap.Error(err))
	}

	st, err := storage.NewLocalStorage(appConfig.LocalStoragePath, true)

	if err != nil {
		logger.Fatal("Failed to create storage", zap.Error(err))
	}

	finalPorts := processPorts(scanInput.Ports)
	finalPaths := processPaths(scanInput.Paths)

	logger.Info("Starting scanning process...")
	// TO DO : Setup proxy settings, skipping for now

	hostnames := make([]string, len(scanInput.Hostnames))
	for i, hc := range scanInput.Hostnames {
		hostnames[i] = hc.Hostname
	}
	scanner := scanner.NewScanner(logger, st)
	scanner.ScanHosts(hostnames, finalPorts, finalPaths)
	logger.Info("Scanning completed.")
}

func readPortsFromFile(filePath string) ([]int, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var ports []int
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		portStr := strings.TrimSpace(scanner.Text())
		port, err := strconv.Atoi(portStr)
		if err != nil {
			return nil, err
		}
		ports = append(ports, port)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	return ports, nil
}

func readPathsFromFile(filePath string) ([]string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var paths []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		path := strings.TrimSpace(scanner.Text())
		paths = append(paths, path)
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}

	return paths, nil
}

func processPorts(portsConfig PortsConfig) []int {
	var result []int
	portSet := make(map[int]bool)

	if portsConfig.ScanCommon {
		commonPorts, err := readPortsFromFile("../port-list.txt")
		if err != nil {
			logger.Error("Failed to read common ports",
				zap.String("file", "../port-list.txt"),
				zap.Error(err))
			return nil
		}
		for _, p := range commonPorts {
			portSet[p] = true
		}
	}

	for _, p := range portsConfig.Include {
		portSet[p] = true
	}
	for _, p := range portsConfig.Exclude {
		delete(portSet, p)
	}
	for p := range portSet {
		result = append(result, p)
	}

	return result
}

func processPaths(pathsConfig PathsConfig) []string {
	pathSet := make(map[string]bool)
	var result []string

	if pathsConfig.ScanCommon {
		commonPaths, err := readPathsFromFile("../path-list.txt")
		if err != nil {
			logger.Error("Failed to read common paths",
				zap.String("file", "../path-list.txt"),
				zap.Error(err))
			return nil
		}
		for _, p := range commonPaths {
			pathSet[p] = true
		}
	}

	for _, p := range pathsConfig.Include {
		pathSet[p] = true
	}
	for _, p := range pathsConfig.Exclude {
		delete(pathSet, p)
	}

	for p := range pathSet {
		result = append(result, p)
	}

	// logger.Info("Processed paths",
	// 	zap.Strings("finalPaths", result),
	// )

	return result
}
