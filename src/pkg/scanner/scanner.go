package scanner

import (
	"archive/zip"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"strings"
	"sync"
	"time"

	"zero-config-api-discovery/pkg/storage"

	"github.com/getkin/kin-openapi/openapi3"
	"github.com/go-openapi/loads"
	"github.com/go-openapi/validate"
	"github.com/google/uuid"
	"go.uber.org/zap"
)

type Scanner struct {
	Logger              *zap.Logger
	Storage             storage.Storage
	DiscoveredAPISpecs  []APISpecMetadata
	SpecContents        map[string][]byte
	TotalHostsScanned   int
	TotalOpenPortsFound int
	httpClient          http.Client
	mu                  sync.Mutex
}

type APISpecMetadata struct {
	Host      string `json:"host"`
	Port      int    `json:"port"`
	PathFound string `json:"path_found"`
	SpecType  string `json:"specType"`
	SpecFile  string `json:"specFile"`
}

func NewScanner(logger *zap.Logger, st storage.Storage) *Scanner {
	return &Scanner{
		Logger:       logger,
		Storage:      st,
		SpecContents: make(map[string][]byte),
		httpClient: http.Client{
			Timeout: 2 * time.Second,
		},
	}
}

func (s *Scanner) ScanHosts(hosts []string, ports []int, paths []string) {
	startTime := time.Now()
	var wg sync.WaitGroup

	for _, host := range hosts {
		wg.Add(1)
		go func(h string) {
			defer wg.Done()
			s.scanHost(h, ports, paths)
		}(host)
	}
	wg.Wait()

	if len(s.DiscoveredAPISpecs) > 0 {
		endTime := time.Now()
		zipBytes, err := s.createZipWithManifestInMemory(startTime, endTime)
		if err != nil {
			s.Logger.Error("Error creating zip file with manifest", zap.Error(err))
			return
		}

		zipFileName := uuid.New().String() + ".zip"
		if err := s.storeZipFile(s.Storage, zipFileName, zipBytes); err != nil {
			s.Logger.Error("Error storing zip file", zap.Error(err))
			return
		}
	} else {
		s.Logger.Info("No API specs discovered, skipping zip creation.")
	}
}

func (s *Scanner) scanHost(host string, ports []int, paths []string) {
	s.TotalHostsScanned++
	openPorts := s.scanPorts(host, ports)
	for port := range openPorts {
		s.checkAndStorePaths(host, port, paths, s.determineSupportedProtocols(host, port))
	}
}

func (s *Scanner) determineSupportedProtocols(host string, port int) []string {
	s.Logger.Debug("Checking supported protocols", zap.String("host", host), zap.Int("port", port))
	protocols := []string{}
	for _, protocol := range []string{"https", "http"} {
		url := fmt.Sprintf("%s://%s:%d", protocol, host, port)
		s.Logger.Debug("Sending request to check protocol", zap.String("url", url))
		resp, err := s.httpClient.Get(url)
		if err == nil {
			s.Logger.Debug("Found supported protocol", zap.String("protocol", protocol), zap.String("host", host), zap.Int("port", port))
			protocols = append(protocols, protocol)
		}
		if resp != nil {
			resp.Body.Close()
		}
	}
	return protocols
}

func (s *Scanner) scanPorts(host string, ports []int) <-chan int {
	openPorts := make(chan int, len(ports))
	var wg sync.WaitGroup

	for _, port := range ports {
		wg.Add(1)
		go func(p int) {
			defer wg.Done()
			address := fmt.Sprintf("%s:%d", host, p)
			conn, err := net.DialTimeout("tcp", address, 1*time.Second)
			if err == nil {
				conn.Close()
				openPorts <- p
				s.TotalOpenPortsFound++
			} else {
				s.Logger.Debug("Failed to connect", zap.Int("port", p), zap.Error(err))
			}
		}(port)
	}

	go func() {
		wg.Wait()
		close(openPorts)
	}()

	return openPorts
}

func (s *Scanner) checkAndStorePaths(host string, port int, paths []string, supportedProtocols []string) {
	for _, path := range paths {
		for _, protocol := range supportedProtocols {
			if s.tryCheckAndStorePath(protocol, host, port, path) {
				// If the path check is successful with this protocol, no need to try other protocols for the same path
				break
			}
		}
	}
}

func (s *Scanner) tryCheckAndStorePath(protocol, host string, port int, path string) bool {
	url := fmt.Sprintf("%s://%s:%d%s", protocol, host, port, path)
	resp, err := s.httpClient.Get(url)
	if err != nil || resp.StatusCode != http.StatusOK {
		s.Logger.Debug("Error or Notfound", zap.String("host", host), zap.String("path", path), zap.Int("port", port), zap.Error(err))
		if resp != nil {
			resp.Body.Close()
		}
		return false
	}
	defer resp.Body.Close()

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		s.Logger.Error("Error reading response body", zap.String("url", url), zap.Error(err))
		return false
	}

	if loader := openapi3.NewLoader(); loader != nil {
		loader.IsExternalRefsAllowed = true
		doc3, err := loader.LoadFromData(bodyBytes)
		if err == nil && doc3 != nil {
			if err := doc3.Validate(loader.Context); err == nil {
				specVersion := "OpenAPI3"
				return s.storeValidSpec(host, port, path, specVersion, bodyBytes)
			}
		}
	}
	doc2, err := loads.Analyzed(bodyBytes, "2.0")
	if err == nil && doc2 != nil {
		errs := validate.Spec(doc2, nil)
		if errs == nil {
			specVersion := "OpenAPI2"
			return s.storeValidSpec(host, port, path, specVersion, bodyBytes)
		} else {
			s.Logger.Error("OpenAPI 2.0 validation failed", zap.String("url", url), zap.String("errors", errs.Error()))
		}
	}

	s.Logger.Error("Failed to validate OpenAPI specification", zap.String("url", url))
	return false
}

func (s *Scanner) storeValidSpec(host string, port int, path string, specVersion string, bodyBytes []byte) bool {
	fileName := sanitizeFileName(fmt.Sprintf("%s-%d%s", host, port, path))

	s.mu.Lock()
	s.DiscoveredAPISpecs = append(s.DiscoveredAPISpecs, APISpecMetadata{
		Host:      host,
		Port:      port,
		PathFound: path,
		SpecType:  specVersion,
		SpecFile:  fileName,
	})
	s.mu.Unlock()

	s.SpecContents[fileName] = bodyBytes
	return true
}

func sanitizeFileName(fileName string) string {
	return strings.ReplaceAll(fileName, "/", "-")
}

func (s *Scanner) createZipWithManifestInMemory(startTime, endTime time.Time) ([]byte, error) {
	buf := new(bytes.Buffer)
	zipWriter := zip.NewWriter(buf)

	for _, spec := range s.DiscoveredAPISpecs {
		writer, err := zipWriter.Create(spec.SpecFile)
		if err != nil {
			return nil, err
		}

		// Retrieve the actual spec content from the map
		specContent, exists := s.SpecContents[spec.SpecFile]
		if !exists {
			s.Logger.Error("Spec file content missing", zap.String("file", spec.SpecFile))
			continue
		}

		if _, err := writer.Write(specContent); err != nil {
			return nil, err
		}
	}

	manifest := s.generateManifest(startTime, endTime)
	manifestBytes, err := json.MarshalIndent(manifest, "", "  ")
	if err != nil {
		return nil, err
	}

	manifestWriter, err := zipWriter.Create("manifest.json")
	if err != nil {
		return nil, err
	}
	if _, err := manifestWriter.Write(manifestBytes); err != nil {
		return nil, err
	}

	if err := zipWriter.Close(); err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func (s *Scanner) storeZipFile(st storage.Storage, zipFilePath string, zipBytes []byte) error {
	if err := st.Store(zipFilePath, zipBytes); err != nil {
		s.Logger.Error("Error storing zip file", zap.Error(err))
		return err
	}

	return nil
}

func (s *Scanner) generateManifest(startTime, endTime time.Time) map[string]interface{} {
	s.mu.Lock()
	defer s.mu.Unlock()

	return map[string]interface{}{
		"summary": map[string]interface{}{
			"startTime":               startTime.Format(time.RFC3339),
			"endTime":                 endTime.Format(time.RFC3339),
			"totalHostsScanned":       s.TotalHostsScanned,
			"totalOpenPortsFound":     s.TotalOpenPortsFound,
			"totalAPISpecsDiscovered": len(s.DiscoveredAPISpecs),
		},
		"discoveredAPISpecs": s.DiscoveredAPISpecs,
	}
}
