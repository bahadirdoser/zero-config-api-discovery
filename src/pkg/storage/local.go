package storage

import (
	"os"
	"path/filepath"
)

type LocalStorage struct {
	BasePath string
}

func NewLocalStorage(basePath string, ensureDir bool) (*LocalStorage, error) {
	if _, err := os.Stat(basePath); os.IsNotExist(err) {
		if ensureDir {
			err := os.MkdirAll(basePath, 0755)
			if err != nil {
				return nil, err
			}
		} else {
			return nil, err
		}
	} else if err != nil {
		return nil, err
	}

	return &LocalStorage{BasePath: basePath}, nil
}

func (l *LocalStorage) Store(path string, data []byte) error {

	fullPath := filepath.Join(l.BasePath, path)
	return os.WriteFile(fullPath, data, 0644)
}
