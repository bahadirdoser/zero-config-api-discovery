package storage

import (
    "bytes"
    "context"
    "github.com/aws/aws-sdk-go-v2/aws"
    "github.com/aws/aws-sdk-go-v2/config"
    "github.com/aws/aws-sdk-go-v2/service/s3"
)

type S3Storage struct {
    Client     *s3.Client
    BucketName string
}

func NewS3Storage(bucketName string) (*S3Storage, error) {
    cfg, err := config.LoadDefaultConfig(context.TODO())
    if err != nil {
        return nil, err
    }

    client := s3.NewFromConfig(cfg)
    return &S3Storage{Client: client, BucketName: bucketName}, nil
}

func (s *S3Storage) Store(path string, data []byte) error {
    _, err := s.Client.PutObject(context.TODO(), &s3.PutObjectInput{
        Bucket: aws.String(s.BucketName),
        Key:    aws.String(path),
        Body:   bytes.NewReader(data),
    })
    return err
}
