package config

import (
	"encoding/json"
	"os"
)

type AWSConfig struct {
	Region          string `json:"region"`
	AccessKeyID     string `json:"accessKeyID,omitempty"`
	SecretAccessKey string `json:"secretAccessKey,omitempty"`
	S3BucketName    string `json:"s3BucketName"`
}
type AppConfig struct {
	DeploymentType   string    `json:"deploymentType"`
	LocalStoragePath string    `json:"localStoragePath"`
	AWS              AWSConfig `json:"aws"`
	Logging          LogConfig `json:"logging"`
}

type LogConfig struct {
    Level            string   `json:"level"`
    OutputPaths      []string `json:"outputPaths"`
    ErrorOutputPaths []string `json:"errorOutputPaths"`
}

func LoadConfig(path string) (*AppConfig, error) {
    file, err := os.Open(path)
    if err != nil {
        return nil, err
    }
    defer file.Close()

    decoder := json.NewDecoder(file)
    config := &AppConfig{}
    if err := decoder.Decode(config); err != nil {
        return nil, err
    }

    return config, nil
}
